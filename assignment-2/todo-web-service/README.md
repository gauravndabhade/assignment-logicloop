# Assignment 2

> ## Prerequisite

- python 3.7
- MongoDB 3.6.3

### Setup envirnment

```shell
$ python3.7 -m venv env
$ source env/bin/activate
```

### Install dependancies & configure settings

```shell
(env)$ pip install -r requirememnts.txt
(env)$
(env)$ cat .env

MONGO_URI=mongodb://0.0.0.0:27017/db_todo
JWT_SECRET_KEY=asdf;lkj_qwerty][poiu
DEBUG=True
```

### Start server

```shell
(env)$ python app.py   # Server will start on http://0.0.0.0:5000/

```

> ## Setup with docker-compose

```shell
$ docker-compose build
$ docker-compose up
```
