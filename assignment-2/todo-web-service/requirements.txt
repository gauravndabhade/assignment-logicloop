Flask==1.1.2
Flask-RESTful==0.3.8
Flask-PyMongo==2.3.0
python-dotenv==0.14.0
Flask-JWT-Extended==3.24.1
