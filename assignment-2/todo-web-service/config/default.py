import os

from dotenv import load_dotenv

load_dotenv(verbose=True)

class Config(object):
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
    MONGO_URI = os.getenv("MONGO_URI")
    DEBUG = (os.getenv("DEBUG") == 'True')
