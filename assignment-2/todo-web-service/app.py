import json

from bson import json_util
from bson.objectid import ObjectId
from flask import Flask, Response, request
from flask_jwt_extended import (JWTManager, create_access_token,
                                get_jwt_identity, jwt_required)
from flask_pymongo import PyMongo
from flask_restful import Api, Resource, abort, reqparse

app = Flask(__name__)

app.config.from_object("config.default.Config")

jwt = JWTManager(app)

api = Api(app)

mongo = PyMongo(app)


def abort_if_todo_doesnt_exist(todo_id):
    query = {"id": todo_id}
    if mongo.db.todo.find(query).count():
        abort(404, message="Todo {} doesn't exist".format(todo_id))


class GenerateToken(Resource):
    def post(self):
        if not request.is_json:
            return Response(json.dumps({"msg": "Missing JSON in request"}), 400)

        username = request.json.get("username", None)
        password = request.json.get("password", None)
        if not username:
            return Response(json.dumps({"msg": "Missing username parameter"}), 400)
        if not password:
            return Response(json.dumps({"msg": "Missing password parameter"}), 400)

        if username != "admin" or password != "admin":
            return Response(json.dumps({"msg": "Bad username or password"}), 401)

        # Identity can be any data that is json serializable
        access_token = create_access_token(identity=username)
        return Response(json.dumps({"access_token": access_token}), 200)


class Todo(Resource):
    @jwt_required
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        query = {"_id": ObjectId(todo_id)}
        task = mongo.db.todo.find_one(query)
        return Response(json.dumps(task, default=json_util.default), 200)

    @jwt_required
    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        query = {"_id": ObjectId(todo_id)}
        _ = mongo.db.todo.delete_one(query)
        return Response("", 204)

    @jwt_required
    def put(self, todo_id):
        task = request.get_json(force=True)
        print(task)
        query = {"_id": ObjectId(todo_id)}
        result = mongo.db.todo.update(query, task)
        return Response(json.dumps(result), 201)


class TodoList(Resource):
    @jwt_required
    def get(self):
        tasks = mongo.db.todo.find()
        return Response(json_util.dumps(tasks), 200)

    @jwt_required
    def post(self):
        task = request.get_json(force=True)
        print(task)
        task = mongo.db.todo.insert(task)
        return Response(json.dumps(task, default=json_util.default), 200)


api.add_resource(GenerateToken, "/todo/api/v1.0/token/")
api.add_resource(TodoList, "/todo/api/v1.0/")
api.add_resource(Todo, "/todo/api/v1.0/<todo_id>/")

if __name__ == "__main__":
    app.run(host="0.0.0.0")
