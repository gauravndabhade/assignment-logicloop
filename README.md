# Assignment Logicloop

This assignment is divided into 2 parts as follows

## Assignment 1 ([**Click here**](assignment-1/README.md))

> 1. Python script to construct pattern

```shell
    a.  *
        * *
        * * *
        * * * *
        * * * * *

    b.  *
        * *
        * * *
        * * * *
        * * * * *
        * * * *
        * * *
        * *
        *
```

> 2. Print the factorial of a number using a for loop

> 3. Python script using nested for loop to create a chess board, use table width = 270px and take 30px as cell height and width. You need to output HTML in a file on the local file system which when opened in the browser looks like a chess board.

## Assignment 2 ([**Click here**](assignment-2/todo-web-service/README.md))

> 1. Designing a simple web service

    The task of designing a web service or API that adheres to the REST guidelines then becomes an exercise in identifying the resources that will be exposed and how they will be affected by the different request methods. Let's say we want to write a To Do List application and we want to design a web service for it. The first thing to do is to decide what is the root URL to access this service. For example, we could expose this service as:

> http://[`hostname`]/todo/api/v1.0/

| HTTP Method | URI                                             | Action                  |
| ----------- | ----------------------------------------------- | ----------------------- |
| GET         | http://[hostname]/todo/api/v1.0/tasks           | Retrieve list of tasks  |
| GET         | http://[hostname]/todo/api/v1.0/tasks/[task_id] | Retrieve a task         |
| POST        | http://[hostname]/todo/api/v1.0/tasks           | Create a new task       |
| PUT         | http://[hostname]/todo/api/v1.0/tasks/[task_id] | Update an existing task |
| DELETE      | http://[hostname]/todo/api/v1.0/tasks/[task_id] | Delete a task           |
| POST        | http://[hostname]/todo/api/v1.0/token/          | Get token for `admin`   |
|             |                                                 |                         |

[Click here](assignment-2/todo-web-service/logicloop-assignment-documentation.pdf) to check postman documentation for service

> A task as having the following fields:

```python
# This is a example of task object
task = {
    'id' : 1,
    'title': 'Sample Task',
    'description': 'This is a demo task',
    'done': False
}
```

## Bonus:

- [ ] Error handling.

- [x] Securing your API using HTTP basic authentication.
