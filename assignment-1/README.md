# Assignment 1

## Patterns

```shell
$ cd patterns
$ python first_pattern.py
*
* *
* * *
* * * *
* * * * *
$
$
$ python second_pattern.py
*
* *
* * *
* * * *
* * * * *
* * * *
* * *
* *
*
```

## Factorials

```shell
$ cd factorials
$ python factorials.py
enter a number: 5
factorial of  5  is as follows :
1 * 2 * 3 * 4 * 5 = 120
```

## chessboard

```shell
$ cd chessboard
$ pip install -r requirements.txt
$
$ python chessboard.py  # It will open chrome to display chessboard
$
```
