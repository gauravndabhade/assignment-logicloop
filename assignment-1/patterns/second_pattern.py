from first_pattern import FirstPattern


class SecondPattern(FirstPattern):
    def _lower_trangle(self, N):
        """
        Display trangle pattern

        * * * *
        * * *
        * *
        *

        Args:
            N (int): Number of rows  
        """    

        for r in range(N - 1, 0, -1):
            for t in range(r):
                print("*", end=" ")
            print()

    def display(self, stars):
        self._upper_trangle(stars)
        self._lower_trangle(stars)

if __name__ == "__main__":
    stars = 5
    SecondPattern().display(stars)
