class FirstPattern:
    def _upper_trangle(self, N):
        """
        Display trangle pattern

        *
        * *
        * * *
        * * * *
        * * * * *

        Args:
            N (int): Number of rows  
        """    
        for r in range(1, N + 1, 1):
            for t in range(r):
                print("*", end=" ")
            print()

            
    def display(self, stars):
        self._upper_trangle(stars)

if __name__ == "__main__":
    stars = 5
    FirstPattern().display(stars)    
