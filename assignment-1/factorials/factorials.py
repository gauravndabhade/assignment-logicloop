
def fact(num):
    print("factorial of ", num, " is as follows :")
    fac = 1
    for i in range(1, num + 1):
        fac = fac * i
        print(str(i), end="")
        if i == num:
            print(" = ", end="")
        else:
            print(" * ", end="")
    print(fac)


if __name__ == "__main__":
    num = int(input("enter a number: "))    
    fact(num)
