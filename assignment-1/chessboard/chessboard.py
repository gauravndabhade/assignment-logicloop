import os
import webbrowser

import dominate
from dominate.tags import *

doc = dominate.document(title='Welcome to chessboard!')

def get_color(i, j):
    """
    Get color for chessboard boxes
    Args:
        i (int): position of box in row
        j (int): position of box in col

    Returns:
        str: Color `black` or `white`
    """    
    if i % 2 == 0:
        if j % 2 == 0:
            return 'black'
        else:
            return 'white'
    else:
        if j % 2 == 0:
            return 'white'
        else:
            return 'black'

with doc.head:
    link(rel='stylesheet', href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css')
    script(type='text/javascript', src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js')
    script(type='text/javascript', src='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js')

with doc:
    with div(cls="container"):
        h1("Welcome to ChessBoard!!")
        
        with table().add(tbody()):
            for i in range(8):
                with tr(style="height:30px; border: 1px solid #ddd;"):
                    for j in range(8):
                        td(style="width:30px;background:"+get_color(i, j))

with open("chessboard.html", "w") as file:
    file.write(str(doc))
    webbrowser.get(using='google-chrome').open(os.path.dirname(__file__) + file.name, new=2)
